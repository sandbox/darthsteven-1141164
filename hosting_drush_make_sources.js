(function($) {
  Drupal.behaviors.hosting_drush_make_source = function(context) {
    $(context).find('.hosting-drush-make-source:not(.hosting-drush-make-source-processed)').each(function() {
      
      $(this)
        .bind('change', function() {
          $form = $(this).parents('form');
          $form.find('.hosting-drush-make-target').val($(this).val());
          $form.find('.hosting-drush-make-target').trigger('change');
        })
        .addClass('hosting-drush-make-source-processed');
    });
  }
})(jQuery);
