<?php
 
 
/**
 * Configuration form for backup schedules
 */
function hosting_drush_make_sources_settings_form() {

  $folder = variable_get('hosting_drush_make_sources_folder', '');
  $form['hosting_drush_make_sources_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Source folder'),
    '#default_value' => $folder,
    '#description' => t('Select a folder in which .make files will be searched for and presented to people making platforms.'),
    '#element_validate' => array('hosting_drush_make_sources_settings_form_folder_validate'),
  );

  return system_settings_form($form);
}

function hosting_drush_make_sources_settings_form_folder_validate($element, &$form_state) {
  $value = $element['#value'];
  if (!empty($value) && !file_exists($value)) {
    form_error($element, t('Please specify a valid folder'));
  }
}